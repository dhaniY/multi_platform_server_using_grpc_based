const PROTO_PATH = `${__dirname}/public/assets/proto/helloworld.proto`
const HOST = 'localhost'
const PORT = 9090

var grpc = require('grpc')

class GRPC {
    constructor() {
        this.hello = grpc.load(PROTO_PATH).helloworld
        this.client = new this.hello.Greeter(`${HOST}:${PORT}`, grpc.credentials.createInsecure())
    }

    run() {
        this.client.sayHello({ name: 'Anshor' }, {}, (err, response) => {
            console.log(response.message)
        })
    }
}

module.exports = GRPC