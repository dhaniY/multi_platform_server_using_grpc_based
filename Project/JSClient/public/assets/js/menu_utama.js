var background, background2
var name_field
var btn_play
var panel

class Menu_Utama extends Phaser.Scene 
{
	constructor() {
		super({ key: "Menu_Utama" })
	}

	create() {
		background = this.physics.add.sprite(0, 0, 'background').setOrigin(0, 0)
		background2 = this.physics.add.sprite(0, 720, 'background').setOrigin(0, 0)

		background.body.setVelocity(0, -300)
		background2.body.setVelocity(0, -300)

		panel = this.add.group()

		btn_play = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY + 50, 'btn_play')
			.setScale(1)
			.setOrigin(0.5, 0.5)
			.setInteractive()
			.on('pointerup', () => this.openGame())
		btn_play.originScale = btn_play.scaleX

		panel.add(btn_play)
	}

	update() {
		if (background.y <= -720) {
			background.setY(720)
		}

		if (background2.y <= -720) {
			background2.setY(720)
		}

		this.pressEffect(btn_play)		
	}

	pressEffect(object) {
		object.on('pointerdown', function (pointer) {
			object.setScale(0.9 * object.originScale)
		})

		object.on('pointerup', function (pointer) {
			object.setScale(object.originScale)
		})
	}

	hideComponent() {
		btn_play.setInteractive(false)
		panel.toggleVisible(false)
	}

	openGame() {
		this.hideComponent()
		Game.scene.start('Game_Play', true, true)
	}
}