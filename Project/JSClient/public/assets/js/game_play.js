var timer_text
var timerCount = 7
var timer
var quest_list = []
var controller = null
var btn_correct
var btn_wrong
var panel
var playerAnswer = false
var player_list = []

class Controller {
	constructor(text, answer) {
		this.text = text
		this.answer = answer
	}

	setQuestText(text) {
		this.text.setText(text)
	}

	setQuestAnswer(answer) {
		this.answer = answer
	}
}

class Quest {
	constructor(question, answer = false) {
		this.question = question
		this.answer = answer
	}
}

class Game_Play extends Phaser.Scene 
{
	constructor() {
		super({ key: "Game_Play" })
	}

	create() {
		panel = this.add.group()

		var board = this.add.sprite(640, 130, 'quest_board')
			.setOrigin(0.5, 0.5)
			.setScale(0.3)

		timer_text = this.add.text(640, 185, timerCount, { 
			font: 'bold 24px Arial', 
			fill: '#fff',
			align: 'center',
			wordWrap: { width: 300 }
		}).setOrigin(0.5, 0.5)
		timer_text.timerCount = timerCount

		var quest_text = this.add.text(640, 110, "Question", { 
			font: 'bold 22px Arial', 
			fill: '#fff',
			align: 'center',
			wordWrap: { width: 300 }
		}).setOrigin(0.5, 0.5)

		btn_correct = this.add.sprite(500, 640, 'btn_correct')
			.setOrigin(0.5, 0.5)
			.setInteractive()
			.setScale(0.12)
			.on('pointerup', () => this.movePlayer())
		btn_correct.originScale = btn_correct.scaleX

		btn_wrong = this.add.sprite(780, 640, 'btn_wrong')
			.setOrigin(0.5, 0.5)
			.setInteractive()
			.setScale(0.12)
			.on('pointerup', () => this.movePlayer())
		btn_wrong.originScale = btn_wrong.scaleX

		panel.add(board)
		panel.add(timer_text)
		panel.add(quest_text)
		panel.add(btn_correct)
		panel.add(btn_wrong)

		controller = new Controller(quest_text, false)

		this.initialize()
		this.declareAnim()
		this.loopTimer()
		this.showQuest()
		this.socketInit()
	}

	update() {
		this.pressEffect(btn_correct)
		this.pressEffect(btn_wrong)
	}

	pressEffect(object) {
		object.on('pointerdown', function (pointer) {
			object.setScale(0.9 * object.originScale)
		})

		object.on('pointerup', function (pointer) {
			object.setScale(object.originScale)
		})
	}

	initialize() {
		quest_list.push(new Quest("Apakah Nama Saya Anshor ?", true))
		quest_list.push(new Quest("Apakah Kucing Makan Ikan ?", true))
		quest_list.push(new Quest("Apakah Sapi minum Susu ?", true))
		quest_list.push(new Quest("Ular dan Burung sama-sama bertelur, jadi Apakah Ular bisa Terbang ?", false))
		quest_list.push(new Quest("Apa Indonesia terdiri dari Pulau ?", false))
		quest_list.push(new Quest("Apa kamu punya pacar ?", true))
		quest_list.push(new Quest("Apa saya punya pacar ?", false))
		quest_list.push(new Quest("Apa Kucing kamu sudah mandi ?", false))
		quest_list.push(new Quest("Apa saya bisa lulus ?", false))
		quest_list.push(new Quest("Apakah kakak saya bisa lulus ?", true))
	}

	declareAnim() {
		this.anims.create({
			key: 'cowAnim',
			frames: this.anims.generateFrameNumbers('cow', { start: 0, end: 5 }),
			frameRate: 10,
			repeat: -1
		})

		this.anims.create({
			key: 'spriteAnim1',
			frames: this.anims.generateFrameNumbers('monster1', { start: 0, end: 1 }),
			frameRate: 10,
			repeat: -1
		})

		this.anims.create({
			key: 'spriteAnim2',
			frames: this.anims.generateFrameNumbers('monster2', { start: 0, end: 1 }),
			frameRate: 10,
			repeat: -1
		})

		this.anims.create({
			key: 'spriteAnim3',
			frames: this.anims.generateFrameNumbers('monster3', { start: 0, end: 1 }),
			frameRate: 10,
			repeat: -1
		})

		this.anims.create({
			key: 'spriteAnim4',
			frames: this.anims.generateFrameNumbers('monster4', { start: 0, end: 1 }),
			frameRate: 10,
			repeat: -1
		})
	}

	random(min, max) {
		return Math.floor(Math.random() * ( max - min + 1 ) + min)
	}

	loopTimer() {
		timer = this.time.addEvent({ delay: 1000, callback: function() {
			
			if (timer_text.timerCount <= 0) {
				timer_text.timerCount = timerCount
				this.showBull(!controller.answer)
			}
			else {
				timer_text.timerCount -= 1
			}
			
			timer_text.setText(timer_text.timerCount)

		}, callbackScope: this, loop: true })
	}

	showQuest() {
		var index = this.random(0, quest_list.length - 1)

		controller.setQuestText(quest_list[index].question)
		controller.setQuestAnswer(quest_list[index].answer)
		quest_list.splice(index, 1)
	}

	showBull(status) {
		timer.paused = true
		
		// If status is true, bull with summoned in left
		var x = (status) ? 320 : 960
		
		var bull = this.physics.add.sprite(x, 670, 'bull')
			.setScale(1.2)
			.setOrigin(0.5, 0.5)
			.setVelocityY(-500)
		bull.anims.play('cowAnim', true)
		
		panel.toggleVisible(false)
		
		this.time.addEvent({
			delay: 2000,
			callback: function() {
				this.showQuest()
				panel.toggleVisible(true)
				bull.destroy()
				timer.paused = false
			},
			callbackScope: this
		})
	}

	addPlayer(playerInfo, playerName = null) {
		var playerSprite = this.physics.add.sprite(0, 0, 'sprite' + playerInfo.avatarType)
			.setOrigin(0.5, 0.5)
			.setScale(playerInfo.scale)
		playerSprite.anims.play('spriteAnim' + playerInfo.avatarType, true)
		
		var name = this.add.text(-22, 30,  playerName != null ? playerName : 'Other', {
			font: 'bold 15px arial',
			fill: playerName != null ? '#f5ce10' : '#fff',
			align: 'center'
		})
		
		var player = this.add.container(playerInfo.position.x, playerInfo.position.y, [ playerSprite, name ])
		player.side = playerInfo.position.side
		player.index = playerInfo.position.index
		player.playerId = playerInfo.playerId

		player_list.push(player)
	}

	getPlayerFromList(side, index) {
		return player_list.filter((value) => {
			if (value.side == side && value.index == index) {
				return value
			}
		})
	}

	movePlayer() {
		this.socket.emit('movePlayer')
	}
	
	socketInit() {
		// Socket
		var self = this
		this.socket = io()

		this.socket.on('currentPlayers', function (players) {
			Object.keys(players).forEach(function (id) {
				if (players[id].playerId === self.socket.id) {
					self.addPlayer(players[id], "It's Me")
				}
				else {
					self.addPlayer(players[id])
				}
			})
		})
		
		this.socket.on('newPlayer', function (playerInfo) {
			self.addPlayer(playerInfo)
		})

		this.socket.on('updatePosition', function (playerInfo) {
			console.log(`client: ${new Date()}`)
			player_list.forEach(function (otherPlayer) {
				if (playerInfo.playerId === otherPlayer.playerId) {
					self.tweens.add({
						targets: otherPlayer,
						x: playerInfo.position.x,
						y: playerInfo.position.y,
						duration: 500
					})
				}
			})
			
		})
		
		this.socket.on('disconnect', function (playerId) {
			console.log('disconnected')
			player_list.forEach(function (otherPlayer) {
				if (playerId === otherPlayer.playerId) {
					otherPlayer.destroy()
				}
			})
		})
	}
}