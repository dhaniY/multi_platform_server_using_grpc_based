var Game = new Phaser.Game({
    type: Phaser.AUTO,  
    width: 1280,
    height: 720,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false
        }
    },
    backgroundColor: "#0f0f39",
 	scene: [ Splash, Menu_Utama, Game_Play ]
})