class Splash extends Phaser.Scene
{
    constructor() {
        super({ key: "Splash" })
    }

    preload() {
        this.load.image('splash_logo', './assets/images/pens.png')
        this.load.image('background', './assets/images/background.png')
        this.load.image('quest_board', './assets/images/box.png')
        this.load.image('btn_play', './assets/images/btn_play.png')
        this.load.image('btn_correct', './assets/images/btn_correct.png')
        this.load.image('btn_wrong', './assets/images/btn_wrong.png')
        this.load.image('name_field', './assets/images/name_field.png')

        this.load.spritesheet('cow', 
            './assets/images/cow.png',
            { frameWidth: 610, frameHeight: 610 }
        )

        this.load.spritesheet('monster1', 
            './assets/images/monster1.png',
            { frameWidth: 214, frameHeight: 281 }
        )
        this.load.spritesheet('monster2', 
            './assets/images/monster2.png',
            { frameWidth: 214, frameHeight: 281 }
        )
        this.load.spritesheet('monster3', 
            './assets/images/monster3.png',
            { frameWidth: 214, frameHeight: 281 }
        )
        this.load.spritesheet('monster4', 
            './assets/images/monster4.png',
            { frameWidth: 214, frameHeight: 281 }
        )
    }

    create() {
        var splash_logo = this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'splash_logo')
            .setScale(0.8)
            .setOrigin(0.5, 0.5)
            .setAlpha(0)

        this.add.tween({
            targets: splash_logo,
            ease: 'Sine.easeOut',
            duration: 1000,
            delay: 0,
            alpha: {
                getStart: () => 0,
                getEnd: () => 1
            },
            onComplete: () => {
                this.start()
            }
        })
    }

    start() {
        this.scene.remove()
        Game.scene.start('Menu_Utama', true, true)
    }
}