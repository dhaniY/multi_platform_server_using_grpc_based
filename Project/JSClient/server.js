const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io').listen(server)

var players = {}
var correctSide = fillArray(16)
var wrongSide = fillArray(16)
var scale = 0.2
var typeCount = 4

class Position {
	constructor(side, posIndex) {
		this.side = side
		this.index = posIndex
		this.x = side ? (180 + 90 * (parseInt(posIndex / 4))) : (810 + 90 * (parseInt(posIndex / 4)))
		this.y = 500 - 85 * (posIndex % 4)
	}
}

function random(min, max) {
    return Math.floor(Math.random() * ( max - min + 1 ) + min)
}

function fillArray(n) {
    var arr = Array.apply(null, Array(n))
    return arr.map(x => 0)
}

function getEmptyPosition(side) {
    var selectedSide = side ? correctSide : wrongSide

    // Get Empty Place
    var avail = selectedSide.map((value, index) => {
        if (value == 0) {
            return index
        }
    }).filter(element => element !== undefined)

    var availIndex = random(0, avail.length - 1)
    var index = avail[availIndex]

    selectedSide[index] = 1

    return new Position(side, index)
}

function sideLength(side) {
    var selectedSide = side ? correctSide : wrongSide
    var avail = selectedSide.map((value, index) => {
        if (value == 1) {
            return index
        }
    }).filter(element => element !== undefined)

    return avail.length
}

function randomBool() {
    var rand = random(0, 1)
    var correctLength = sideLength(true)
    var wrongLength = sideLength(false)

    if (correctLength >= 16 && wrongLength >= 16) {
        return null
    }

    if (rand == 1 && correctLength >= 16) {
        return false
    }
    else if (rand == 0 && wrongLength >= 16) {
        return true
    }

    return rand
}

app.use(express.static(__dirname + '/public'))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html')
})

io.on('connection', function (socket) {
    players[socket.id] = {
        playerId: socket.id,
        position: getEmptyPosition(randomBool()),
        avatarType: random(1, typeCount),
        scale: scale
    }

    socket.emit('currentPlayers', players)

    socket.broadcast.emit('newPlayer', players[socket.id])

    socket.on('movePlayer', function () {
        var selectedSide = players[socket.id].position.side ? correctSide : wrongSide
        var oppositeSide = players[socket.id].position.side ? wrongSide : correctSide

        selectedSide[players[socket.id].position.index] = 0
        players[socket.id].position = getEmptyPosition(!players[socket.id].position.side)
        oppositeSide[players[socket.id].position.index] = 1

        if (players[socket.id].team === 'red') {
          scores.red += 10
        }

        io.emit('updatePosition', players[socket.id])
    })
    
    socket.on('disconnect', function () {
        console.log('user disconnected: ', socket.id)
        var selectedSide = players[socket.id].position.side ? correctSide : wrongSide
        selectedSide[players[socket.id].position.index] = 0
        delete players[socket.id]
        io.emit('disconnect', socket.id)
    })
})

server.listen(8080, function () {
    console.log(`Listening on ${server.address().port}`)
})