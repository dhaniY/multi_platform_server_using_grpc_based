package com.pens.keblinger.scenes

import com.badlogic.gdx.Gdx
import com.pens.keblinger.GDXGame
import com.pens.keblinger.pref.Assets
import com.pens.keblinger.parent.AutoSprite
import com.pens.keblinger.parent.SceneAdapter

class MenuScene(game: GDXGame?) : SceneAdapter(game!!) {

    private var btnStart: AutoSprite? = null

    override fun show() {
        super.show()

        Gdx.gl.glClearColor(83.toFloat() / 255, 104.toFloat() / 255,
                211.toFloat() / 255, 1f)
        initStart()
    }

    override fun render(delta: Float) {
        super.render(delta)

        game.batch.begin()

        btnStart!!.draw(game.batch)

        game.batch.end()

        inputHandler()
    }

    private fun initStart() {
        btnStart = AutoSprite(game, Assets.btnStart)
        btnStart!!.setScale(0.5f)
        btnStart!!.setPosition(btnStart!!.centerX(), btnStart!!.centerY())
    }

    private fun inputHandler() {
        if (!Gdx.input.justTouched()) return

        touchPoint.set(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)
        camera.unproject(touchPoint)

        if (btnStart!!.boundingRectangle.contains(touchPoint.x, touchPoint.y)) {
            game.screen = PlayScene(game)
        }
    }
}