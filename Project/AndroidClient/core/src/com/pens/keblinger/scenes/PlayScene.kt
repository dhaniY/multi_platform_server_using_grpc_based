package com.pens.keblinger.scenes

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.Align
import com.pens.keblinger.GDXGame
import com.pens.keblinger.animation.Parallax
import com.pens.keblinger.pref.Assets
import com.pens.keblinger.parent.AutoSprite
import com.pens.keblinger.parent.SceneAdapter
import com.pens.keblinger.parent.SpriteSheet
import com.pens.keblinger.pref.Settings
import com.badlogic.gdx.utils.Timer
import com.badlogic.gdx.utils.Timer.Task

class PlayScene(game: GDXGame?) : SceneAdapter(game!!) {

    lateinit var fontQuest: BitmapFont
    lateinit var fontTimer: BitmapFont

    private var bg1: Parallax? = null
    private var bg2: Parallax? = null

    private var box: AutoSprite? = null

    private lateinit var glyphQuest: GlyphLayout
    private var questIndex: Int = 0

    private val glyphTimer = GlyphLayout()
    private var timerCount: Int = 7
    private lateinit var timer: Task

    private var btnCorrect: AutoSprite? = null
    private var btnWrong: AutoSprite? = null

    private var cow: SpriteSheet? = null
    private var monster: SpriteSheet? = null

    private var playerAnswer: Boolean = true
    private var monsPos: Int = 1

    private var move: Boolean = false
    private var timesup: Boolean = false
    private val speed: Float = 5f

    override fun show() {
        super.show()

        fontQuest = Assets.getFont("fonts/FredokaOne.ttf",
                Settings.QUEST_SIZE - ((2560f - game.height.toFloat()) / 440f).toInt())
        fontTimer = Assets.getFont("fonts/FredokaOne.ttf",
                Settings.TIMER_SIZE - ((2560f - game.height.toFloat()) / 440f).toInt())

        initBackground()
        initBox()
        initMonster((0..3).shuffled().first())
        initCow()
        initCorrect()
        initWrong()
        initTimer()

        timer.run()
    }

    override fun render(delta: Float) {
        super.render(delta)

        game.batch.begin()

        bg1!!.draw(game.batch)
        bg2!!.draw(game.batch)

        monster!!.draw(game.batch)
        cow!!.draw(game.batch)

        if (!timesup) {
            btnCorrect!!.draw(game.batch)
            btnWrong!!.draw(game.batch)
            box!!.draw(game.batch)

            fontQuest.draw(game.batch, glyphQuest,
                    game.width.toFloat() / 2 - box!!.getScaledWidth() * 0.9f / 2,
                    box!!.y + box!!.getScaledHeight() / 2 + glyphQuest.height / 2 +
                            (50f * box!!.getOriginScale()))

            fontTimer.draw(game.batch, glyphTimer,
                    game.width.toFloat() / 2 - glyphTimer.width / 2,
                    box!!.y + (160f * box!!.getOriginScale()))
        }

        game.batch.end()

        bg1!!.move()
        bg2!!.move()

        inputHandler()
        moveHandler()
        cowHandler()
    }

    fun getOriginScale(value: Float): Float {
        return value * game.height.toFloat() / Settings.ORIGIN_HEIGHT
    }

    private fun initBackground() {
        bg1 = Parallax(game, Assets.background)
        bg1!!.setPosition(bg1!!.centerX(), 0f)

        bg2 = Parallax(game, Assets.background)
        bg2!!.setPosition(bg2!!.centerX(), bg2!!.getScaledHeight() - 20)
    }

    private fun initBox() {
        box = AutoSprite(game, Assets.box)
        box!!.setScale(0.9f)
        box!!.setOrigin(box!!.width / 2, 0f)
        box!!.setPosition(box!!.centerX(), game.height.toFloat() -
                (750f * game.height.toFloat() / Settings.ORIGIN_HEIGHT))

        glyphQuest = GlyphLayout(fontQuest, Assets.quests[questIndex].question, Color.WHITE,
                box!!.getScaledWidth() * 0.9f, Align.center, true)
        glyphTimer.setText(fontTimer, timerCount.toString())
    }

    private fun initCow() {
        cow = SpriteSheet(game, 0.5f, Assets.cow)
        cow!!.setScale(0.75f)
        cow!!.setPosition(getCowX(), -cow!!.getScaledHeight())
    }

    private fun initMonster(index: Int = 0) {
        val type: Array<TextureRegion> = when(index) {
            0 -> Assets.monster1
            1 -> Assets.monster2
            2 -> Assets.monster3
            else -> Assets.monster1
        }

        monster = SpriteSheet(game, 0.5f, type)
        monster!!.setScale(0.1f)
        monster!!.setPosition(getMonsX(), getMonsY())
    }

    private fun initCorrect() {
        btnCorrect = AutoSprite(game, Assets.btnCorrect)
        btnCorrect!!.setScale(0.25f)
        btnCorrect!!.setOrigin(btnCorrect!!.width / 2, 0f)
        btnCorrect!!.setPosition(btnCorrect!!.centerX() + game.width.toFloat() / 4,
                250f * btnCorrect!!.getOriginScale())
    }

    private fun initWrong () {
        btnWrong = AutoSprite(game, Assets.btnWrong)
        btnWrong!!.setScale(0.25f)
        btnWrong!!.setOrigin(btnWrong!!.width / 2, 0f)
        btnWrong!!.setPosition(btnWrong!!.centerX() - game.width.toFloat() / 4,
                250f * btnWrong!!.getOriginScale())
    }

    private fun getCowX(): Float {
        val posX = game.width.toFloat() / 4 - cow!!.halfWidth()

        return if (Assets.quests[questIndex].answer)
            return posX else posX + game.width.toFloat() / 2
    }

    private fun getMonsX(): Float {
        val posX = (game.width.toFloat() * (((monsPos - 1) % 3) + 1) / 8) -
                monster!!.halfWidth()

        return if (!playerAnswer) return posX else posX + game.width.toFloat() / 2
    }

    private fun getMonsY(): Float {
        return getOriginScale(700f) + (monsPos / 4) * getOriginScale(200f) *
                game.height.toFloat() / Settings.ORIGIN_HEIGHT
    }

    private fun inputHandler() {
        if (!Gdx.input.justTouched() or timesup) return

        touchPoint.set(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)
        camera.unproject(touchPoint)

        if (btnCorrect!!.boundingRectangle.contains(touchPoint.x, touchPoint.y) && !playerAnswer) {
            playerAnswer = true
            monsPos = (1..20).shuffled().first()
            move = true
        }
        else if (btnWrong!!.boundingRectangle.contains(touchPoint.x, touchPoint.y) && playerAnswer) {
            playerAnswer = false
            monsPos = (1..20).shuffled().first()
            move = true
        }
    }

    private fun moveHandler() {
        if (!move) return

        monster!!.translate((getMonsX() - monster!!.x) * Gdx.graphics.deltaTime * speed,
                (getMonsY() - monster!!.y) * Gdx.graphics.deltaTime * speed)

        if (monster!!.x == getMonsX() && monster!!.y == getMonsY()) {
            move = false
        }
    }

    private fun cowHandler() {
        if (!timesup) return

        cow!!.y += (game.height.toFloat() + cow!!.getScaledHeight()) / 2 * Gdx.graphics.deltaTime

        if (cow!!.y >= game.height + cow!!.getScaledHeight()) {
            cow!!.setPosition(getCowX(), -cow!!.getScaledHeight())

            timesup = false

            initTimer()
            timer.run()
        }
    }

    private fun initTimer() {
        timer = Timer.schedule(object : Task() {
            override fun run() {
                if (timerCount <= 0) {
                    questIndex = (0 until Assets.quests.size - 1).shuffled().first()
                    glyphQuest = GlyphLayout(fontQuest, Assets.quests[questIndex].question, Color.WHITE,
                            box!!.getScaledWidth() * 0.9f, Align.center, true)

                    timerCount = 10
                    timesup = true

                    timer.cancel()
                }
                else {
                    timerCount -= 1
                }

                glyphTimer.setText(fontTimer, timerCount.toString())
            }
        }, 1f, 1f)
    }
}