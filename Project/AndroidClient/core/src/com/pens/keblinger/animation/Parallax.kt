package com.pens.keblinger.animation

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.pens.keblinger.GDXGame
import com.pens.keblinger.parent.AutoSprite

class Parallax(private val game: GDXGame, texture: Texture): AutoSprite(game, texture) {

    var speed: Float = 20f

    init {
        setOrigin(width / 2, 0f)
        speed *= game.height.toFloat() / 2560f
    }

    fun move() {
        y -= speed

        if (y <= -getScaledHeight() + 20)
            y = (getScaledHeight() - 20)
    }
}