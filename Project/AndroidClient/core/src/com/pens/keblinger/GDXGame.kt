package com.pens.keblinger

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.pens.keblinger.interfaces.AndroidServices
import com.pens.keblinger.pref.Assets
import com.pens.keblinger.scenes.MenuScene

class GDXGame(private val androidServices: AndroidServices) : Game() {

    lateinit var batch: SpriteBatch
    public lateinit var android: AndroidServices

    var width: Int = 0
    var height: Int = 0

    override fun create() {
        batch = SpriteBatch()
        android = androidServices

        Assets.load()

        width = Gdx.graphics.width
        height = Gdx.graphics.height

        setScreen(MenuScene(this))
    }

    override fun render() {
        super.render()
    }

    override fun dispose() {
        super.dispose()
    }
}
