package com.pens.keblinger.interfaces

interface AndroidServices {
    fun debug(tag: String, text: String)
    fun toast(text: String)
}
