package com.pens.keblinger.pref

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.pens.keblinger.`object`.Quest

object Assets {

    lateinit var background: Texture
    lateinit var box: Texture

    lateinit var btnStart: Texture
    lateinit var btnCorrect: Texture
    lateinit var btnWrong: Texture

    lateinit var cowSprite: Texture
    lateinit var cow: Array<TextureRegion>

    lateinit var monster1Sprite: Texture
    lateinit var monster1: Array<TextureRegion>

    lateinit var monster2Sprite: Texture
    lateinit var monster2: Array<TextureRegion>

    lateinit var monster3Sprite: Texture
    lateinit var monster3: Array<TextureRegion>

    lateinit var monster4Sprite: Texture
    lateinit var monster4: Array<TextureRegion>

    lateinit var quests: Array<Quest>

    fun load() {
        background = Texture("background_mobile.png")
        box = Texture("box.png")

        btnStart = Texture("btnStart.png")
        btnCorrect = Texture("btnCorrect.png")
        btnWrong = Texture("btnWrong.png")

        cowSprite = Texture("cow.png")
        cow = arrayOf(
                TextureRegion(cowSprite, 0, 0, 610, 610),
                TextureRegion(cowSprite, 611, 0, 610, 610),
                TextureRegion(cowSprite, 1221, 0, 610, 610),
                TextureRegion(cowSprite, 0, 611, 610, 610),
                TextureRegion(cowSprite, 611, 611, 610, 610),
                TextureRegion(cowSprite, 1221, 611, 610, 610)
        )

        monster1Sprite = Texture("monster1.png")
        monster1 = arrayOf(
                TextureRegion(monster1Sprite, 0, 0, 214, 281),
                TextureRegion(monster1Sprite, 215, 0, 214, 281)
        )

        monster2Sprite = Texture("monster2.png")
        monster2 = arrayOf(
                TextureRegion(monster2Sprite, 0, 0, 214, 281),
                TextureRegion(monster2Sprite, 215, 0, 214, 281)
        )

        monster3Sprite = Texture("monster3.png")
        monster3 = arrayOf(
                TextureRegion(monster3Sprite, 0, 0, 214, 281),
                TextureRegion(monster3Sprite, 215, 0, 214, 281)
        )

        monster4Sprite = Texture("monster4.png")
        monster4 = arrayOf(
                TextureRegion(monster3Sprite, 0, 0, 214, 281),
                TextureRegion(monster3Sprite, 215, 0, 214, 281)
        )

        quests = arrayOf(
                Quest("Apakah Nama Saya Anshor ?", true),
                Quest("Apakah Kucing Makan Ikan ?", true),
                Quest("Apakah Sapi minum Susu ?", true),
                Quest("Ular dan Burung sama-sama bertelur, jadi " +
                        "Apakah Ular bisa Terbang ?", false),
                Quest("Apa Indonesia terdiri dari Pulau ?", false),
                Quest("Apa kamu punya pacar ?", true),
                Quest("Apa saya punya pacar ?", false),
                Quest("Apa Kucing kamu sudah mandi ?", false),
                Quest("Apa saya bisa lulus ?", false),
                Quest("Apakah kakak saya bisa lulus ?", true)
        )
    }

    fun getFont(path: String, size: Int): BitmapFont {
        val generator = FreeTypeFontGenerator(
                Gdx.files.internal(path)
        )
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.size = (size * Gdx.graphics.density).toInt()
        val font = generator.generateFont(parameter)
        generator.dispose()

        return font
    }
}
