package com.pens.keblinger.pref

object Settings {
    var APP_HOST = "http://localhost/"
    var APP_PORT = 80

    var QUEST_SIZE = 18
    var TIMER_SIZE = 24
    var ORIGIN_HEIGHT = 2560f
}