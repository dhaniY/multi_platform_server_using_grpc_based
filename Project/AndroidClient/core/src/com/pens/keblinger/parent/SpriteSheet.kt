package com.pens.keblinger.parent

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.pens.keblinger.GDXGame

open class SpriteSheet(val game: GDXGame, private val frameDuration: Float, var keyFrames: Array<TextureRegion>) : Sprite() {

    private var textureRegion: TextureRegion? = null
    private var stateTime: Float = 0.toFloat()

    init {
        stateTime = 0f

        this.setScale(1f)
    }

    override fun setScale(scaleXY: Float) {
        super.setScale(game.width.toFloat() / keyFrames[0].regionWidth.toFloat() * scaleXY)
    }

    fun getScaledWidth(): Float {
        return keyFrames[0].regionWidth.toFloat() * scaleX
    }

    fun getScaledHeight(): Float {
        return keyFrames[0].regionHeight.toFloat() * scaleX
    }

    fun halfWidth(): Float {
        return getScaledWidth() / 2
    }

    fun centerX(): Float {
        return game.width.toFloat() / 2 - halfWidth()
    }

    override fun draw(batch: Batch) {
        stateTime += Gdx.graphics.deltaTime
        textureRegion = getKeyFrame(stateTime)

        batch.draw(textureRegion, x, y,
                textureRegion!!.regionWidth.toFloat() * scaleX,
                textureRegion!!.regionHeight.toFloat() * scaleX)

        setBounds(x, y,
                textureRegion!!.regionWidth.toFloat() * scaleX,
                textureRegion!!.regionHeight.toFloat() * scaleX)
    }

    fun getKeyFrame(stateTime: Float): TextureRegion {
        val frameNumber = (stateTime * frameDuration).toInt() % keyFrames.size
        return keyFrames[(0..1).shuffled().first()]
    }
}