package com.pens.keblinger.parent

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.pens.keblinger.GDXGame
import com.pens.keblinger.pref.Settings

open class AutoSprite(private val game: GDXGame, texture: Texture) : Sprite(texture) {

    var scaleAuto: Float = 1f

    init {
        this.setScale(1f)
    }

    override fun setScale(scaleXY: Float) {
        scaleAuto = scaleXY
        super.setScale(game.width.toFloat() / width * scaleXY)
    }

    fun centerX(): Float {
        return (game.width.toFloat() / 2 - width / 2)
    }

    fun centerY(): Float {
        return (game.height.toFloat() / 2 - height / 2)
    }

    fun getScaledWidth(): Float {
        return width * scaleX
    }

    fun getScaledHeight(): Float {
        return height * scaleX
    }

    fun getOriginScale(): Float {
        return game.height.toFloat() / Settings.ORIGIN_HEIGHT
    }


}
