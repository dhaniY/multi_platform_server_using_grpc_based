package com.pens.keblinger.parent

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector3
import com.pens.keblinger.GDXGame

open class SceneAdapter(protected var game: GDXGame) : ScreenAdapter() {

    protected var camera: OrthographicCamera
    protected var touchPoint: Vector3

    init {
        camera = OrthographicCamera(game.width.toFloat(), game.height.toFloat())
        camera.position.set(game.width.toFloat() / 2, game.height.toFloat() / 2, 0f)

        touchPoint = Vector3()
    }

    override fun render(delta: Float) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        camera.update()
        game.batch.projectionMatrix = camera.combined
    }
}
