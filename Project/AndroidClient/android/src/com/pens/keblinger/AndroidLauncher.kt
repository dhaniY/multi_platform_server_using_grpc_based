package com.pens.keblinger

import android.os.Bundle

import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.pens.keblinger.interfaces.AndroidServices

class AndroidLauncher : AndroidApplication() {

    private var androidServices: AndroidServices? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = AndroidApplicationConfiguration()
        config.useImmersiveMode = true

        androidServices = Manager(this)

        initialize(GDXGame(androidServices!!), config)
    }
}
