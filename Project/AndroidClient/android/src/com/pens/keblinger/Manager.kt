package com.pens.keblinger

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.Toast

import com.pens.keblinger.interfaces.AndroidServices
import com.pens.keblinger.pref.Settings
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import java.util.concurrent.TimeUnit

class Manager(private val context: Context) : AndroidServices {
    override fun debug(tag: String, text: String) {
        Log.d(tag, text)
    }

    override fun toast(text: String) {
        (context as Activity).runOnUiThread { Toast.makeText(context, text, Toast.LENGTH_SHORT).show() }
    }
}
