import { Body, Controller, HttpCode, Post, Logger, Next } from '@nestjs/common';
import {
  Client,
  ClientGrpc,
  GrpcMethod,
  GrpcStreamCall,
  GrpcStreamMethod,
  Transport,
} from '@nestjs/microservices';
import { grpcClientOptions } from './grpc.game.options';
import { Observable, of, interval } from 'rxjs';
import {map, throttle, timeout} from 'rxjs/operators';

interface Player {
    id :string;
}


@Controller('game')
export class GameController {
    @Client(grpcClientOptions)
    client: ClientGrpc;

    static timemaster: number = 7;
    static question: number = Math.floor((Math.random() * 10) + 1);
    static players: Player[] = [];

    @Post()
    @HttpCode(200)
    call(@Body() data: Player) {
      return this.AddPlayer(data);
    }

    @GrpcMethod('GameService')
    AddPlayer(data: Player) :Player[] {
        GameController.players.push(data);
        return GameController.players;
    }

    @GrpcStreamMethod('GameService')
    async PlayStream(data: Observable<any>): Promise<any> {

      return new Promise<any>((resolve, reject) => {
        data
        .pipe(
            throttle(val => interval(1000)),
            map(()=> {
                GameController.timemaster -= 1;

                if (GameController.timemaster <= -1) {
                    GameController.timemaster = 7;
                    GameController.question = Math.floor((Math.random() * 10) + 1);
                }
            })
        )
        .subscribe(
          msg => {
            resolve({
              timer: {
                  time: GameController.timemaster,
                  question: GameController.question
              },
              player: GameController.players,
            });
          },
          err => {
            reject(err);
          },
        );
      });
  }
    
}
