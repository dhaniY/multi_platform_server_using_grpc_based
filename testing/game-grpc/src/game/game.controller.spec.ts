import * as ProtoLoader from '@grpc/proto-loader';
import { INestApplication, Logger } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { fail } from 'assert';
import { expect } from 'chai';
import * as GRPC from 'grpc';
import { join } from 'path';
import * as request from 'supertest';
import { GameController } from './game.controller';
import { async } from 'rxjs/internal/scheduler/async';

interface Player {
  id :string;
}

describe('GRPC transport', () => {
  let server;
  let app: INestApplication;
  let client: any;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      controllers: [GameController],
    }).compile();

    app = module.createNestApplication();
    server = app.getHttpAdapter().getInstance();

    app.connectMicroservice({
      transport: Transport.GRPC,
      options: {
        package: 'gamegrpc',
        protoPath: join(__dirname, './gamegrpc.proto'),
      },
    });
    // Start gRPC microservice
    await app.startAllMicroservicesAsync();
    await app.init();
    // Load proto-buffers for test gRPC dispatch
    const proto = ProtoLoader.loadSync(
      join(__dirname, './gamegrpc.proto'),
    ) as any;
    // Create Raw gRPC client object
    const protoGRPC = GRPC.loadPackageDefinition(proto) as any;
    // Create client connected to started services at standard 5000 port
    client = new protoGRPC.gamegrpc.GameService(
      'localhost:5000',
      GRPC.credentials.createInsecure(),
    );
  });

  it(`GRPC Running method addPlayer`, async() => {
    let date = new Date();
    Logger.log('Begin', date.getMilliseconds().toLocaleString());
    let data = await request(server)
      .post('/game')
      .send({id:"12345"})
      .expect(200, [{id:"12345"}]);
    Logger.log('Time Response :', date.getMilliseconds().toLocaleString());  
  });

  // it('GRPC Sending and receiving Stream from RX handler', async () => {
  //   const callHandler = client.PlayStream();

  //   callHandler.on('data', (msg: any) => {
  //     expect(msg).to.eql(
  //       { 
  //         player : [{
  //           id: "12345"
  //         }],
  //         timer: {
  //           question: 9,
  //           time: 6
  //         } 
  //       });
  //     callHandler.cancel();
  //   });

  //   callHandler.on('error', (err: any) => {
  //     // We want to fail only on real errors while Cancellation error
  //     // is expected
  //     if (
  //       String(err)
  //         .toLowerCase()
  //         .indexOf('cancelled') === -1
  //     ) {
  //       fail('gRPC Stream error happened, error: ' + err);
  //     }
  //   });

  //   return new Promise((resolve, reject) => {
  //     callHandler.write({ data: [{id:"1234"}, {id:"1243"}, {id:"1342"}, {id:"4321"}, {id:"3214"}] });
  //     setTimeout(() => resolve(), 1000);
  //   });
  // });

//   it('GRPC Sending and receiving Stream from Call Passthrough handler', async () => {
//     const callHandler = client.SumStreamPass();

//     callHandler.on('data', (msg: number) => {
//       expect(msg).to.eql({ result: 15 });
//       callHandler.cancel();
//     });

//     callHandler.on('error', (err: any) => {
//       // We want to fail only on real errors while Cancellation error
//       // is expected
//       if (
//         String(err)
//           .toLowerCase()
//           .indexOf('cancelled') === -1
//       ) {
//         fail('gRPC Stream error happened, error: ' + err);
//       }
//     });

//     return new Promise((resolve, reject) => {
//       callHandler.write({ data: [1, 2, 3, 4, 5] });
//       setTimeout(() => resolve(), 1000);
//     });
//   });

//   afterAll(async () => {
//     await app.close();
//     client.close();
//   });
 });