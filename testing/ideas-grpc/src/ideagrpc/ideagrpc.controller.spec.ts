import { Test, TestingModule } from '@nestjs/testing';
import { IdeagrpcController } from './ideagrpc.controller';

describe('Ideagrpc Controller', () => {
  let controller: IdeagrpcController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IdeagrpcController],
    }).compile();

    controller = module.get<IdeagrpcController>(IdeagrpcController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
