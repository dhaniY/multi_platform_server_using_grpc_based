import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IdeagrpcEntity } from './ideagrpc.entity';
import { Repository } from 'typeorm';
import { IIdea } from './interface/idea.interface';
import { IIdeaById } from './interface/idea-by-id.interface';

interface Empty {}
@Injectable()
export class IdeagrpcService {
    // Using Dependency Injection for entitiy idea grpc
    constructor(
        @InjectRepository(IdeagrpcEntity)
        private ideagrpcRepositoy :Repository<IdeagrpcEntity> 
    ){}

    // Define asynchronous service get all data on database
    async showAll(data: Empty) {
        const ideas: IIdea[] =  await this.ideagrpcRepositoy.find();
        return {ideas};
    }

    // Define asynchronous service get one data on database
    async read(data: IIdeaById) {
        const idea: any = await this.ideagrpcRepositoy.findOne(data.id);
        return idea;
    }

    // Define asynchronous service create data on database
    async create(data :IIdea) {
        const idea :any = this.ideagrpcRepositoy.create(data);
        await this.ideagrpcRepositoy.save(idea);
        return idea;
    }

    // Define asynchronous service update data on database
    async update(dataId :IIdeaById, data :Partial<IIdea>) {
        await this.ideagrpcRepositoy.update(dataId.id, data);
        return await this.ideagrpcRepositoy.findOne({where: { id: dataId.id }});
    }

    // Define asynchronous service delete data on database
    async destroy(data :IIdeaById) {
        await this.ideagrpcRepositoy.delete(data.id);
        return { delete : true};
    }
}
