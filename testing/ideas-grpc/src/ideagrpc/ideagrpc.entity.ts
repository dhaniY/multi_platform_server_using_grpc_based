import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column } from 'typeorm';

@Entity('ideagrpc')
export class IdeagrpcEntity {
    @PrimaryGeneratedColumn('uuid')
    id :string;

    @CreateDateColumn()
    created :Date;

    @Column('text')
    idea :string;

    @Column('text')
    description :string;
}