import { Test, TestingModule } from '@nestjs/testing';
import { IdeagrpcService } from './ideagrpc.service';

describe('IdeagrpcService', () => {
  let service: IdeagrpcService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IdeagrpcService],
    }).compile();

    service = module.get<IdeagrpcService>(IdeagrpcService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
