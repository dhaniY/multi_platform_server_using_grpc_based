export interface IIdea {
    idea: string;
    description: string;
}