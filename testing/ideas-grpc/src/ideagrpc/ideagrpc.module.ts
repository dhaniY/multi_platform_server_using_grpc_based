import { Module } from '@nestjs/common';
import { IdeagrpcController } from './ideagrpc.controller';
import { IdeagrpcService } from './ideagrpc.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IdeagrpcEntity } from './ideagrpc.entity';
// Load all dependency for service ideagrpc
@Module({
  imports: [TypeOrmModule.forFeature([IdeagrpcEntity])], // Using TypeOrm to load entity IdeagrpcEntity that using postgres
  controllers: [IdeagrpcController], // load controller for ideagrpc service
  providers: [IdeagrpcService], 
  exports: [IdeagrpcService],
})
export class IdeagrpcModule {}
