import { Controller, OnModuleInit, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { GrpcMethod, Client, ClientGrpc } from '@nestjs/microservices';
import { IIdeaById } from './interface/idea-by-id.interface';
import { IIdea } from './interface/idea.interface';
import { IdeagrpcService } from './ideagrpc.service';
import { grpcClientOptions } from 'src/grpc.idea.options';
import { Observable, empty } from 'rxjs';
import { Logger } from '@nestjs/common';

// Define interface grpc service on client
interface IdeaService {
    findAll(data: IEmpty): Observable<any>;
    findOne(data: IIdeaById): Observable<any>;
    create(data: IIdea): Observable<any>;
    update(id: IIdeaById, data: IIdea): Observable<any>;
    destroy(data: IIdeaById): Observable<any>;
}

// Define interfece empty for input empty on service fineOne
interface IEmpty {}

// Define service in endpoint '/ideagrpc'
@Controller('/ideagrpc')
// Define controller class that implement interface OnModuleInit
export class IdeagrpcController implements OnModuleInit{
    constructor(private ideagrpcService :IdeagrpcService){}

    // Define Client Grpc
    @Client(grpcClientOptions)
    private readonly client: ClientGrpc;

    // define private variable type interface IdeaService
    private ideaService: IdeaService;

    // define object empty
    private empty :IEmpty = {};

    // define private variable update data to save temporary object Partial<Idea>
    private updateData :Partial<IIdea>;

    // Assign idea service to client grpc service
    onModuleInit(){
        this.ideaService = this.client.getService<IdeaService>('IdeaService');
    }

    // REST: Get All idea 
    @Get()
    readAllIdea(): Observable<any> {
        return this.ideaService.findAll(this.empty);
    }

    // REST: Get One idea
    @Get(':id')
    readIdea(@Param('id') id :string): Observable<any> {
        return this.ideaService.findOne({id});
    }

    // REST: Post idea
    @Post()
    createIdea(@Body() data: IIdea): Observable<any> {
        return this.ideaService.create(data);
    }

    // REST: Update idea
    @Put(':id')
    updateIdea(@Param('id') id: string, @Body() data: IIdea): Observable<any> {
        this.updateData = data;
        return this.ideaService.update({id}, data);
    }

    // REST: Destroy idea
    @Delete(':id')
    deleteIdea(@Param('id') id: string): Observable<any> {
        return this.ideaService.destroy({id});
    }

    // Grpc method findAll() on service IdeaService
    @GrpcMethod('IdeaService')
    findAll() {
        Logger.log('Server: call method findAll', 'IdeaGrpcServices');
        return this.ideagrpcService.showAll(empty);
    }


    // Grpc method findOne() on service IdeaService
    @GrpcMethod('IdeaService')
    findOne(data: IIdeaById) {
        Logger.log('Server: call method findOne', 'IdeaGrpcServices');
        return this.ideagrpcService.read(data);
    }


    // Grpc method create() on service IdeaService
    @GrpcMethod('IdeaService')
    create(data: IIdea) {
        Logger.log('Server: call method create', 'IdeaGrpcServices');
        return this.ideagrpcService.create(data);
    }

    
    // Grpc method update() on service IdeaService
    @GrpcMethod('IdeaService')
    update(id: IIdeaById, data: Partial<IIdea>) {
        Logger.log('Server: call method update', 'IdeaGrpcServices');
        if (this.updateData.description != null && this.updateData.idea != null) {
            const updateData : Partial<IIdea> = {
                "idea": this.updateData.idea,
                "description": this.updateData.description
            }
            return this.ideagrpcService.update(id, updateData);
        } else if (this.updateData.idea != null && this.updateData.description == null) {
            const updateData : Partial<IIdea> = {
                "idea": this.updateData.idea,
            }
            return this.ideagrpcService.update(id, updateData);
        } else if (this.updateData.idea == null && this.updateData.description != null) {
            const updateData : Partial<IIdea> = {
                "description": this.updateData.description,
            }
            return this.ideagrpcService.update(id, updateData);
        } else {
            return {"message" : "No data to update"};
        }
    }


    // Grpc method destroy() on service IdeaService
    @GrpcMethod('IdeaService')
    destroy(id: IIdeaById) {
        Logger.log('Server: call method destroy', 'IdeaGrpcServices');
        return this.ideagrpcService.destroy(id);
    }
}
