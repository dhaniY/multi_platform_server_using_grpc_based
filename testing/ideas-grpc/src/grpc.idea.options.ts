import { ClientOptions } from '@nestjs/microservices';
import { join } from 'path';
import { Transport } from '@nestjs/common/enums/transport.enum';

// Define Options for microservice grpc
export const grpcClientOptions: ClientOptions = {
    transport: Transport.GRPC,
    options: {
        package: 'ideagrpc',
        protoPath: join(__dirname, './ideagrpc/ideagrpc.proto'),
    }
}