import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'dotenv/config';
import { Logger } from '@nestjs/common';
import Eureka from 'eureka-js-client';
import {grpcClientOptions} from './grpc.idea.options';

// define port on .env file
const PORT = process.env.PORT || 3000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // Connect Microservice Grpc
  app.connectMicroservice(grpcClientOptions);
  // Start all microservice
  await app.startAllMicroservicesAsync();
  // Service listen on PORT
  await app.listen(PORT);

 // Log running application
  Logger.log(`Server running at http://localhost:${PORT}`, 'Bootstrap');
}
bootstrap();