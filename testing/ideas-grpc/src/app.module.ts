import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IdeaModule } from './idea/idea.module';
import { IdeagrpcModule } from './ideagrpc/ideagrpc.module';

@Module({
  imports: [TypeOrmModule.forRoot(), IdeaModule, IdeagrpcModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
