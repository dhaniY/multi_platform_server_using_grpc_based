# Efektifitas gRPC dalam penggunaan backend service dalam game.<br>

## Deskripsi
Merupakan sebuah proyek yang dirancang menggunakan framework gRPC untuk mewujudkan arsitektur server berbasi RPC yang memiliki performa tinggi dan meningkatkan efisiensi waktu dan sumber daya dalam pengembangan game server.

### Framework yang digunakan
#### [gRPC](https://grpc.io/)
gRPC adalah <i> framework </i>modern<i> open source </i> berbasis <i> remote procedure call (RPC) </i> yang berjalan menggunakan HTTP / 2, protokol transport jaringan terbaru, dirancang untuk latensi rendah dan permintaan multiplexing melalui koneksi TCP tunggal menggunakan stream.

#### [NestJs](https://docs.nestjs.com/)
Nest atau NestJs adalah <i>framework</i> untuk membangun aplikasi server Node.js yang efisien dan dapat diskalakan. Sepenuhnya mendukung TypeScript dan menggabungkan elemen OOP (Pemrograman Berorientasi Objek), FP (Pemrograman Fungsional), dan FRP (Pemrograman Reaktif Fungsional).

### Alasan Penggunaan gRPC
gRPC memiliki beberapa kelebihan, yaitu :
* gRPC adalah RPC framework buatan google
* Berjalan secara Cross Platform
* Multi Language, yaitu mendukung berbagai bahasa pemrograman seperti Javascript, Java, C#, dan lainnya
* Bi-directional streaming, yaitu klien dapat menggunakan stream untuk melakukan request data dan server dapat mengirim kembali data stream dalam RPC yang sama
* Memiliki Performa yang cepat dan efisien

## Mulai

### Prasyarat
* Node.js (> = 8.9.0)
* PorstgreSql versi 10 atau 11
* Postman

#### <i>Clone</i> Proyek

gunakan git

```bash
https://gitlab.com/dhaniY/multi_platform_server_using_grpc_based.git
```
#### Pindah ke <i> branch </i> testing
```bash
git checkout testing
```

#### Struktur Direktori
```
testing
│   chat_app
│   ideas-grpc    
│   game-grpc
│
└───ideas-grpc
    │   src
    │   test
    │
    └───src
        │   interface
        │   ...
   

```
#### Pindah ke direktory ideagrpc
```brach
cd ./testing/ideas-grpc
```


#### <i> Download dependencies </i>

gunakan [npm](https://www.npmjs.com/) yang sudah terinstall pada sistem operasi Anda

```bash
npm install
```

#### Buat database di postgres
```bash
sudo su - postgres
```
```bash
createdb ideas
```

#### Konfigurasi database
Edit file ormconfig.json yang berada di dalam direktori ./testing/ideagrpc
```bash
{
    "type": "postgres",
    "host": "localhost",
    "port": "5432",
    "username": "postgres",
    "password": "sesuaikan dengan password anda",
    "database": "ideas",
    "synchronize": true,
    "logging": true,
    "entities": ["./src/**/*.entity.ts", "./dist/**/*.entity.js"]
}
```

#### Jalankan Program
Pindah ke direktory ./testing/ideagrpc
Pada terminal jalankan perintah di bawah ini.
```bash
npm start
```

#### Test API
Kami menggunakan Postman untuk melakukan <i> testing unary API </i>, pengujian gRPC API yang kami lakukan menggunakan penghubung RESTFull API. <br>
Setelah dilakukan pemanggilan API dari gRPC, <i> return value </i> dari
<i> method gRPC </i> tersebut kami kembalikan ke fungsi RESTFull API sehingga hasil keluaran API akan terlihat jelas apabila <i> testing </i> API dilakukan dengan menggunakan Postman.

##### Get
Url : localhost:3000/idea
![Screen Shot](Documentation/images/get.png)

##### Post
Url : localhost:3000/idea
![Screen Shot](Documentation/images/post.png)

##### Update
Url : localhost:3000/idea/{uuid}
![Screen Shot](Documentation/images/update.png)

##### Delete
Url : localhost:3000/idea
![Screen Shot](Documentation/images/delete.png)

## Percobaan
### Skenario
Skenario percobaan dilakukan dengan menggunakan unit testing menggunakan framework Jest. <br>
Jest unit testing melakukan request dengan parameter id bertipe string dan akan menerima kembalian response object yang berisi keseluruhan id yang telah terdafatar didalam server. <br>

#### Pindah ke <i> branch </i> testing
```bash
git checkout testing
```

#### Pindah ke direktory game-grpc
```brach
cd ./testing/game-grpc
```

#### <i> Download dependencies untuk game-grpc </i>

gunakan [npm](https://www.npmjs.com/) yang sudah terinstall pada sistem operasi Anda

```bash
npm install
```

#### Jalankan Testing
Pastikan anda berada pada direktory ./testing/game-grpc
Pada terminal jalankan perintah di bawah ini.
```bash
npm test
```

#### Hasil Tes
![Screen Shot](Documentation/images/unit-test.png)
Dari screenshot hasil tes di atas, PASS menunjukkan bahwa tes yang telah dijalankan berhasil tampa adanya error. Tercatat <i> time response </i> server +63ms.




## Contoh Penggunaan Framework
Semua contoh di bawah ini terdapat pada directory testing
### Idea-Grpc
#### Protobuff
Service gRPC didefinisikan menggunakan protocol buffers.
[refensi](https://grpc.io/docs/quickstart/node/)
```bash
syntax = "proto3";
package ideagrpc;

service IdeaService{
    rpc FindOne     (IdeaById) returns (Idea)   {}
    rpc FindAll     (Empty)    returns (Ideas)  {}
    rpc Create      (Idea)     returns (Idea)   {}
    rpc Update      (IdeaById) returns (Idea)   {} 
    rpc Destroy     (IdeaById) returns (Empty)  {}
}

message IdeaById {
    string id = 1;
}

message Empty {}

message Idea {
    string id = 1;
    string idea = 2;
    string description = 3;
    string created = 4;
}

message Ideas {
    repeated Idea ideas = 1;
}
  
```
#### Idea Service
```bash
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IdeagrpcEntity } from './ideagrpc.entity';
import { Repository } from 'typeorm';
import { IIdea } from './interface/idea.interface';
import { IIdeaById } from './interface/idea-by-id.interface';

interface Empty {}
@Injectable()
export class IdeagrpcService {
    // Using Dependency Injection for entitiy idea grpc
    constructor(
        @InjectRepository(IdeagrpcEntity)
        private ideagrpcRepositoy :Repository<IdeagrpcEntity> 
    ){}

    // Define asynchronous service get all data on database
    async showAll(data: Empty) {
        const ideas: IIdea[] =  await this.ideagrpcRepositoy.find();
        return {ideas};
    }

    // Define asynchronous service get one data on database
    async read(data: IIdeaById) {
        const idea: any = await this.ideagrpcRepositoy.findOne(data.id);
        return idea;
    }

    // Define asynchronous service create data on database
    async create(data :IIdea) {
        const idea :any = this.ideagrpcRepositoy.create(data);
        await this.ideagrpcRepositoy.save(idea);
        return idea;
    }

    // Define asynchronous service update data on database
    async update(dataId :IIdeaById, data :Partial<IIdea>) {
        await this.ideagrpcRepositoy.update(dataId.id, data);
        return await this.ideagrpcRepositoy.findOne({where: { id: dataId.id }});
    }

    // Define asynchronous service delete data on database
    async destroy(data :IIdeaById) {
        await this.ideagrpcRepositoy.delete(data.id);
        return { delete : true};
    }
}
```

#### NestJs Controller
```bash
import { Controller, OnModuleInit, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { GrpcMethod, Client, ClientGrpc } from '@nestjs/microservices';
import { IIdeaById } from './interface/idea-by-id.interface';
import { IIdea } from './interface/idea.interface';
import { IdeagrpcService } from './ideagrpc.service';
import { grpcClientOptions } from '../grpc.idea.options';
import { Observable, empty } from 'rxjs';
import { Logger } from '@nestjs/common';

// Define interface grpc service on client
interface IdeaService {
    findAll(data: IEmpty): Observable<any>;
    findOne(data: IIdeaById): Observable<any>;
    create(data: IIdea): Observable<any>;
    update(id: IIdeaById, data: IIdea): Observable<any>;
    destroy(data: IIdeaById): Observable<any>;
}

// Define interfece empty for input empty on service fineOne
interface IEmpty {}

// Define service in endpoint '/ideagrpc'
@Controller('/ideagrpc')
// Define controller class that implement interface OnModuleInit
export class IdeagrpcController implements OnModuleInit{
    constructor(private ideagrpcService :IdeagrpcService){}

    // Define Client Grpc
    @Client(grpcClientOptions)
    private readonly client: ClientGrpc;

    // define private variable type interface IdeaService
    private ideaService: IdeaService;

    // define object empty
    private empty :IEmpty = {};

    // define private variable update data to save temporary object Partial<Idea>
    private updateData :Partial<IIdea>;

    // Assign idea service to client grpc service
    onModuleInit(){
        this.ideaService = this.client.getService<IdeaService>('IdeaService');
    }

    // REST: Get All idea 
    @Get()
    readAllIdea(): Observable<any> {
        return this.ideaService.findAll(this.empty);
    }

    // REST: Get One idea
    @Get(':id')
    readIdea(@Param('id') id :string): Observable<any> {
        return this.ideaService.findOne({id});
    }

    // REST: Post idea
    @Post()
    createIdea(@Body() data: IIdea): Observable<any> {
        return this.ideaService.create(data);
    }

    // REST: Update idea
    @Put(':id')
    updateIdea(@Param('id') id: string, @Body() data: IIdea): Observable<any> {
        this.updateData = data;
        return this.ideaService.update({id}, data);
    }

    // REST: Destroy idea
    @Delete(':id')
    deleteIdea(@Param('id') id: string): Observable<any> {
        return this.ideaService.destroy({id});
    }

    // Grpc method findAll() on service IdeaService
    @GrpcMethod('IdeaService')
    findAll() {
        Logger.log('Server: call method findAll', 'IdeaGrpcServices');
        return this.ideagrpcService.showAll(empty);
    }


    // Grpc method findOne() on service IdeaService
    @GrpcMethod('IdeaService')
    findOne(data: IIdeaById) {
        Logger.log('Server: call method findOne', 'IdeaGrpcServices');
        return this.ideagrpcService.read(data);
    }


    // Grpc method create() on service IdeaService
    @GrpcMethod('IdeaService')
    create(data: IIdea) {
        Logger.log('Server: call method create', 'IdeaGrpcServices');
        return this.ideagrpcService.create(data);
    }

    
    // Grpc method update() on service IdeaService
    @GrpcMethod('IdeaService')
    update(id: IIdeaById, data: Partial<IIdea>) {
        Logger.log('Server: call method update', 'IdeaGrpcServices');
        if (this.updateData.description != null && this.updateData.idea != null) {
            const updateData : Partial<IIdea> = {
                "idea": this.updateData.idea,
                "description": this.updateData.description
            }
            return this.ideagrpcService.update(id, updateData);
        } else if (this.updateData.idea != null && this.updateData.description == null) {
            const updateData : Partial<IIdea> = {
                "idea": this.updateData.idea,
            }
            return this.ideagrpcService.update(id, updateData);
        } else if (this.updateData.idea == null && this.updateData.description != null) {
            const updateData : Partial<IIdea> = {
                "description": this.updateData.description,
            }
            return this.ideagrpcService.update(id, updateData);
        } else {
            return {"message" : "No data to update"};
        }
    }


    // Grpc method destroy() on service IdeaService
    @GrpcMethod('IdeaService')
    destroy(id: IIdeaById) {
        Logger.log('Server: call method destroy', 'IdeaGrpcServices');
        return this.ideagrpcService.destroy(id);
    }
}

```

## Contributors
### Gubu Team PENS
* Dhani Yanuar Erdiansyah
* Rahmat Ansori
* Muhammad Rizal Fauzi Wahyu Putra